import React, { useState, useEffect } from "react";
import { MDBRow, MDBNav, MDBNavItem, MDBAlert } from "mdbreact";

import Product from "../components/product/product";
import Spinner from "../components/common/Spinner";
import Axios from "axios";
import CardArea from "../components/product/CardArea";
const View = () => {
  const [products, setProducts] = useState([]);
  const [categories, setCategories] = useState([]);
  const [cardProducts, setCardProducts] = useState(false);
  const [loading, setLoading] = useState(false);
  const [orderLoading, setOrderLoading] = useState(false);
  const [selectCategory, setSelectCategory] = useState("");
  const [selectedProducts, setSelectedProducts] = useState([]);
  const [successMessage, setSuccessMessage] = useState("");
  const getProducts = async () => {
    try {
      const result = await Axios.get("/product");
      const { data } = await Axios.get("/product/categories");
      setCategories(data.categories);
      setProducts(result.data.products);
      setLoading(true);
    } catch (err) {
      setLoading(true);
    }
  };
  useEffect(() => {
    getProducts();
  }, []);

  const cardHandler = (val) => {
    let cards = [];
    setCardProducts(!cardProducts);
    setSuccessMessage("");
    if (localStorage.getItem("cards")) {
      cards = JSON.parse(localStorage.getItem("cards"));
    }
    if (cards.find((cp) => cp === val)) {
      cards = cards.filter((cd) => cd !== val);
      localStorage.setItem("cards", JSON.stringify(cards));
      return;
    }

    cards.push(val);

    localStorage.setItem("cards", JSON.stringify(cards));
  };

  const removeCardItem = (val) => {
    let cards = [];
    setCardProducts(!cardProducts);
    if (localStorage.getItem("cards")) {
      cards = JSON.parse(localStorage.getItem("cards"));
    }
    cards = cards.filter((cd) => cd !== val);
    localStorage.setItem("cards", JSON.stringify(cards));
  };

  const removeCardItems = () => {
    let cards = [];
    setCardProducts(!cardProducts);
    localStorage.setItem("cards", JSON.stringify(cards));
  };

  const categoryHandler = (e, val) => {
    e.preventDefault();
    if (val === "all") {
      setSelectCategory("");
      setSelectedProducts([]);
      return;
    }
    setSelectCategory(val);
    let updatedProducts = products.filter((pd) => pd.category === val);
    setSelectedProducts(updatedProducts);
  };

  const submitOrder = (val) => {
    setOrderLoading(true);
    try {
      Axios.post("/product/order/create", val);
      setOrderLoading(false);
      setSuccessMessage("Your order successfully done");
      removeCardItems();
    } catch (err) {
      setOrderLoading(false);
    }
  };

  if (!loading) {
    return <Spinner />;
  }

  let finalProducts = [];

  if (selectCategory) {
    finalProducts = selectedProducts;
  } else {
    finalProducts = products;
  }
  let cards = [];
  if (localStorage.getItem("cards")) {
    let cardData = JSON.parse(localStorage.getItem("cards"));
    cardData.forEach((pd) => {
      products.forEach((cd) => {
        if (cd._id === pd) {
          cards.push(cd);
        }
      });
    });
  }

  return (
    <section className="text-center my-5">
      <div className="container">
        <div className="row">
          <div className="col-md-9">
            <h2 className="h1-responsive font-weight-bold text-left my-5">
              Our bestsellers
            </h2>
            {successMessage && (
              <MDBAlert color="success">{successMessage}</MDBAlert>
            )}
            <MDBNav tabs>
              <MDBNavItem>
                <a
                  href="/"
                  className={[
                    "nav-link",
                    !selectCategory ? "active" : null,
                  ].join(" ")}
                  onClick={(e) => categoryHandler(e, "all")}
                >
                  All
                </a>
              </MDBNavItem>
              {categories &&
                categories.map((cg, i) => {
                  return (
                    <MDBNavItem key={i}>
                      <a
                        href="/"
                        className={[
                          "nav-link",
                          selectCategory === cg._id ? "active" : null,
                        ].join(" ")}
                        onClick={(e) => categoryHandler(e, cg._id)}
                      >
                        {cg.title}
                      </a>
                    </MDBNavItem>
                  );
                })}
            </MDBNav>
            <MDBRow>
              {finalProducts &&
                finalProducts.map((it) => {
                  return (
                    <Product
                      item={it}
                      key={it._id}
                      cardHandler={() => cardHandler(it._id)}
                      cardCheck={JSON.parse(
                        localStorage.getItem("cards")
                      ).find((cd) => (cd === it._id ? true : false))}
                    />
                  );
                })}
            </MDBRow>
          </div>
          <div className="col-md-3">
            <CardArea
              cards={cards}
              removeCardItem={removeCardItem}
              removeCardItems={removeCardItems}
              submitOrder={submitOrder}
              orderLoading={orderLoading}
            />
          </div>
        </div>
      </div>
    </section>
  );
};

export default View;
