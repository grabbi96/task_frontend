import React from 'react';
import { MDBSpinner } from 'mdbreact';
const Spinner = () => {
    return (
        <div className="spinner-area">
            <MDBSpinner />
        </div>
    );
};

export default Spinner;
