import React from "react";
import { MDBCol, MDBCard, MDBCardImage, MDBCardBody } from "mdbreact";
const product = ({ item, cardHandler, cardCheck }) => {
  const { title, quantity, photo, price } = item;
  return (
    <MDBCol
      lg="4"
      md="6"
      className={["mb-lg-0", "mb-4", cardCheck ? "active" : null].join(" ")}
    >
      <MDBCard className="align-items-center">
        <MDBCardImage
          src={photo}
          top
          alt="sample photo"
          overlay="white-slight"
        />
        <MDBCardBody className="text-center">
          <a href="#!" className="grey-text">
            <h5>{title}</h5>
          </a>
          <h5>
            <strong>
              <a href="#!" className="dark-grey-text">
                {quantity}
              </a>
            </strong>
          </h5>

          <div className="custom-card-footer">
            <h4 className="font-weight-bold blue-text">
              <strong>{price}$</strong>
            </h4>
            <div className="card-area" onClick={() => cardHandler()}>
              <i className="fas fa-cart-plus"></i>
            </div>
          </div>
        </MDBCardBody>
      </MDBCard>
    </MDBCol>
  );
};

export default product;
