import React from 'react';

const CardItem = ({ item, onRemoveHandler }) => {
    let { title, quantity, photo, price } = item;
    return (
        <div className="card-item">
            <img src={photo} alt="as" />
            <div className="card-item-content">
                <h6>{title}</h6>
                <span>{quantity}</span>
            </div>
            <span className="quantity"></span>
            <p className="price">${price}</p>
            <span className="close" onClick={onRemoveHandler}>
                <i className="far fa-times-circle"></i>
            </span>
        </div>
    );
};

export default CardItem;
