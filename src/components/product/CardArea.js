import React from "react";
import CardItem from "./CardItem";
const CardArea = ({
  cards,
  orderLoading,
  removeCardItem,
  removeCardItems,
  submitOrder,
}) => {
  let total = 0;

  cards.forEach((fe) => {
    total += fe.price;
  });

  const onRemoveHandler = (val) => {
    removeCardItem(val);
  };

  const placeHandler = () => {
    let items = JSON.parse(localStorage.getItem("cards"));
    let data = {
      items,
      totalPrice: total,
    };
    submitOrder(data);
  };
  let placeBtnDisabled = false;

  if (cards.length < 1 || orderLoading) {
    placeBtnDisabled = true;
  }
  return (
    <div className="card card-info-area">
      <div className="card-info-top">
        <div className="card-info-header">
          <h4>Your Card</h4>
          <button className="text-danger" onClick={() => removeCardItems()}>
            Clear
          </button>
        </div>

        <div className="card-info-middle">
          {cards &&
            cards.map((cd, i) => {
              return (
                <CardItem
                  item={cd}
                  key={i}
                  onRemoveHandler={() => onRemoveHandler(cd._id)}
                />
              );
            })}
        </div>
      </div>

      <div className="card-info-bottom">
        <button
          className="btn btn-primary"
          onClick={placeHandler}
          disabled={placeBtnDisabled ? true : false}
        >
          Place Order
        </button>
        <p>Grand total:${total}</p>
      </div>
    </div>
  );
};

export default CardArea;
